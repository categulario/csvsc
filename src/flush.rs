use crate::{
    Headers, RowResult, RowStream,
    error::Result,
};

mod target;

pub use target::{Target, TargetManager};

/// Flushes the rows to the destination specified by a column.
///
/// Fields starting with underscore are not written.
pub struct Flush<I, T> {
    iter: I,
    target: T,
    headers: Headers,
}

impl<I, T> Flush<I, T>
where
    I: RowStream,
{
    pub fn new(iter: I, target: T) -> Result<Flush<I, T>> {
        let headers = iter.headers().clone();

        Ok(Flush { iter, target, headers })
    }
}

pub struct IntoIter<I, T> {
    headers: Headers,
    target: T,
    iter: I,
}


impl<I, T> Iterator for IntoIter<I, T>
where
    I: Iterator<Item = RowResult>,
    T: TargetManager,
{
    type Item = RowResult;

    fn next(&mut self) -> Option<Self::Item> {
        match self.iter.next() {
            Some(Ok(row)) => {
                if let Err(e) = self.target.write_row(&self.headers, &row) {
                    Some(Err(e))
                } else {
                    Some(Ok(row))
                }
            },
            err @ Some(Err(_)) => err,
            None => None,
        }
    }
}

impl<I, T> IntoIterator for Flush<I, T>
where
    I: RowStream,
    T: TargetManager,
{
    type Item = RowResult;

    type IntoIter = IntoIter<I::IntoIter, T>;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter {
            target: self.target,
            headers: self.iter.headers().clone(),
            iter: self.iter.into_iter(),
        }
    }
}

impl<I, T> RowStream for Flush<I, T>
where
    I: RowStream,
    T: TargetManager,
{
    fn headers(&self) -> &Headers {
        &self.headers
    }
}
