use std::ops::AddAssign;
use std::str::FromStr;
use std::fmt::{Display, Debug};

use super::Aggregate;
use crate::{Headers, Row, error};

#[derive(Debug)]
pub struct Sum<T> {
    source: String,
    total: T,
    colname: String,
    default: T,
}

impl<T: Copy> Sum<T> {
    pub fn new(colname: String, source: String, init: T) -> Sum<T> {
        Sum {
            source,
            colname,
            total: init,
            default: init,
        }
    }
}

impl<T> Aggregate for Sum<T>
where
    T: AddAssign + FromStr + Display + Copy,
{
    fn update(&mut self, headers: &Headers, row: &Row) -> error::Result<()> {
        match headers.get_field(row, &self.source) {
            Some(data) => {
                let val = data.parse::<T>().unwrap_or(self.default);
                self.total += val;

                Ok(())
            },
            None => Err(error::Error::ColumnNotFound(self.source.to_string())),
        }
    }

    fn value(&self) -> String {
        self.total.to_string()
    }

    fn colname(&self) -> &str {
        &self.colname
    }
}

#[cfg(test)]
mod tests {
    use super::{Aggregate, Sum};
    use crate::{Row, error};

    #[test]
    fn test_sum() {
        let mut sum = Sum::new("new".into(), "a".into(), 0.0);
        let h = Row::from(vec!["a"]).into();

        let r = Row::from(vec!["3.0"]);
        sum.update(&h, &r).unwrap();
        let r = Row::from(vec!["2"]);
        sum.update(&h, &r).unwrap();
        let r = Row::from(vec![".5"]);
        sum.update(&h, &r).unwrap();

        assert_eq!(sum.value(), "5.5");
    }

    #[test]
    fn test_missing_column() {
        let mut sum = Sum::new("new".into(), "a".into(), 0.0);
        let h = Row::from(vec!["b"]).into();

        let r = Row::from(vec!["3.0"]);

        match sum.update(&h, &r) {
            Err(error::Error::ColumnNotFound(val)) => assert_eq!(val, "a"),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_value_error() {
        let mut sum = Sum::new("new".into(), "a".into(), 0.0);
        let h = Row::from(vec!["a"]).into();

        let r = Row::from(vec!["chicken"]);

        sum.update(&h, &r).unwrap();

        assert_eq!(sum.value(), "0");
    }
}
