use super::Aggregate;
use crate::{Headers, Row, error};

#[derive(Default, Debug)]
pub struct Count {
    total: u64,
    colname: String,
}

impl Count {
    pub fn new(colname: String) -> Count {
        Count {
            colname,
            ..Default::default()
        }
    }
}

impl Aggregate for Count {
    fn update(&mut self, _h: &Headers, _r: &Row) -> error::Result<()> {
        self.total += 1;

        Ok(())
    }

    fn value(&self) -> String {
        self.total.to_string()
    }

    fn colname(&self) -> &str {
        &self.colname
    }
}

#[cfg(test)]
mod tests {
    use super::{Aggregate, Count};
    use crate::Row;

    #[test]
    fn test_count() {
        let mut count = Count::new("new".into());
        let h = Row::new().into();
        let r = Row::new();

        count.update(&h, &r).unwrap();
        count.update(&h, &r).unwrap();
        count.update(&h, &r).unwrap();

        assert_eq!(count.value(), "3");
    }
}
