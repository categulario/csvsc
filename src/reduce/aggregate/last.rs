use super::Aggregate;
use crate::{Headers, Row, error};

#[derive(Default, Debug)]
pub struct Last {
    source: String,
    current: String,
    colname: String,
}

impl Last {
    pub fn new(colname: String, source: String, init: String) -> Last {
        Last {
            source,
            colname,
            current: init,
        }
    }
}

impl Aggregate for Last {
    fn update(&mut self, headers: &Headers, row: &Row) -> error::Result<()> {
        match headers.get_field(row, &self.source) {
            Some(data) => {
                self.current.replace_range(.., data);

                Ok(())
            }
            None => Err(error::Error::ColumnNotFound(self.source.to_string())),
        }
    }

    fn value(&self) -> String {
        self.current.clone()
    }

    fn colname(&self) -> &str {
        &self.colname
    }
}

#[cfg(test)]
mod tests {
    use super::{Aggregate, Last};
    use crate::{Row, error};

    #[test]
    fn test_last() {
        let mut last = Last::new("new".into(), "a".into(), "-".into());
        let h = Row::from(vec!["a"]).into();

        let r = Row::from(vec!["3.0"]);
        last.update(&h, &r).unwrap();
        let r = Row::from(vec!["2"]);
        last.update(&h, &r).unwrap();
        let r = Row::from(vec![".5"]);
        last.update(&h, &r).unwrap();

        assert_eq!(last.value(), ".5");
    }

    #[test]
    fn test_missing_column() {
        let mut last = Last::new("new".into(), "a".into(), "-".into());
        let h = Row::from(vec!["b"]).into();

        let r = Row::from(vec!["3.0"]);

        match last.update(&h, &r) {
            Err(error::Error::ColumnNotFound(val)) => assert_eq!(val, "a"),
            _ => panic!("Test failed"),
        }
    }
}
