use std::ops::MulAssign;
use std::str::FromStr;
use std::fmt::{Display, Debug};

use super::Aggregate;
use crate::{Headers, Row, error};

#[derive(Debug)]
pub struct Mul<T> {
    source: String,
    total: T,
    colname: String,
    default: T,
}

impl<T: Copy> Mul<T> {
    pub fn new(colname: String, source: String, init: T) -> Mul<T> {
        Mul {
            source,
            colname,
            total: init,
            default: init,
        }
    }
}

impl<T> Aggregate for Mul<T>
where
    T: MulAssign + FromStr + Display + Copy,
{
    fn update(&mut self, headers: &Headers, row: &Row) -> error::Result<()> {
        match headers.get_field(row, &self.source) {
            Some(data) => {
                let val = data.parse::<T>().unwrap_or(self.default);
                self.total *= val;

                Ok(())
            }
            None => Err(error::Error::ColumnNotFound(self.source.to_string())),
        }
    }

    fn value(&self) -> String {
        self.total.to_string()
    }

    fn colname(&self) -> &str {
        &self.colname
    }
}

#[cfg(test)]
mod tests {
    use super::{Aggregate, Mul};
    use crate::{Row, error};

    #[test]
    fn test_mul() {
        let mut mul = Mul::new("new".into(), "a".into(), 1.0);
        let h = Row::from(vec!["a"]).into();

        let r = Row::from(vec!["3.0"]);
        mul.update(&h, &r).unwrap();
        let r = Row::from(vec!["2"]);
        mul.update(&h, &r).unwrap();
        let r = Row::from(vec![".5"]);
        mul.update(&h, &r).unwrap();

        assert_eq!(mul.value(), "3");
    }

    #[test]
    fn test_missing_column() {
        let mut mul = Mul::new("new".into(), "a".into(), 1.0);
        let h = Row::from(vec!["b"]).into();

        let r = Row::from(vec!["3.0"]);

        match mul.update(&h, &r) {
            Err(error::Error::ColumnNotFound(val)) => assert_eq!(val, "a"),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_value_error() {
        let mut mul = Mul::new("new".into(), "a".into(), 1.0);
        let h = Row::from(vec!["a"]).into();

        let r = Row::from(vec!["chicken"]);

        mul.update(&h, &r).unwrap();

        assert_eq!(mul.value(), "1");
    }
}
