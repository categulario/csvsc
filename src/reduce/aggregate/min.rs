use std::str::FromStr;
use std::cmp::PartialOrd;
use std::fmt::{Display, Debug};

use super::Aggregate;
use crate::{Headers, Row, error};

#[derive(Debug)]
pub struct Min<T> {
    source: String,
    current: T,
    colname: String,
    default: T,
}

impl<T: Copy> Min<T> {
    pub fn new(colname: String, source: String, init:T) -> Min<T> {
        Min {
            source,
            colname,
            current: init,
            default: init,
        }
    }
}

impl<T> Aggregate for Min<T>
where
    T: FromStr + PartialOrd + Display + Copy,
{
    fn update(&mut self, headers: &Headers, row: &Row) -> error::Result<()> {
        match headers.get_field(row, &self.source) {
            Some(data) => {
                let val: T = data.parse().unwrap_or(self.default);

                if val < self.current {
                    self.current = val;
                }

                Ok(())
            },
            None => Err(error::Error::ColumnNotFound(self.source.to_string())),
        }
    }

    fn value(&self) -> String {
        self.current.to_string()
    }

    fn colname(&self) -> &str {
        &self.colname
    }
}

#[cfg(test)]
mod tests {
    use std::f64;

    use super::{Aggregate, Min};
    use crate::{Row, error};

    #[test]
    fn test_min() {
        let mut min = Min::new("new".into(), "a".into(), f64::INFINITY);
        let h = Row::from(vec!["a"]).into();

        let r = Row::from(vec!["3.0"]);
        min.update(&h, &r).unwrap();
        let r = Row::from(vec!["2"]);
        min.update(&h, &r).unwrap();
        let r = Row::from(vec![".5"]);
        min.update(&h, &r).unwrap();

        assert_eq!(min.value(), "0.5");
    }

    #[test]
    fn test_missing_column() {
        let mut min = Min::new("new".into(), "a".into(), f64::INFINITY);
        let h = Row::from(vec!["b"]).into();

        let r = Row::from(vec!["3.0"]);

        match min.update(&h, &r) {
            Err(error::Error::ColumnNotFound(val)) => assert_eq!(val, "a"),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_value_error() {
        let mut min = Min::new("new".into(), "a".into(), f64::INFINITY);
        let h = Row::from(vec!["a"]).into();

        let r = Row::from(vec!["chicken"]);

        min.update(&h, &r).unwrap();

        assert_eq!(min.value(), "inf");
    }
}
