use super::Aggregate;
use crate::{Headers, Row, error};

pub struct Group {
    contents: Vec<Box<dyn Aggregate>>,
}

impl Group {
    pub fn update(&mut self, headers: &Headers, row: &Row) -> error::Result<()> {
        for agg in self.contents.iter_mut() {
            agg.update(headers, row)?
        }

        Ok(())
    }

    pub fn into_row(self) -> Row {
        let contents: Vec<String> = self.contents.iter().map(|g| g.value()).collect();
        let buff_size = contents.iter().map(|s| s.len()).sum();
        let mut row = Row::with_capacity(buff_size, contents.len());

        for item in contents.into_iter() {
            row.push_field(&item);
        }

        row
    }
}

impl<'a> From<Vec<Box<dyn Aggregate>>> for Group {
    fn from(contents: Vec<Box<dyn Aggregate>>) -> Group {
        Group { contents }
    }
}
