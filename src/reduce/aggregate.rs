use crate::{Headers, Row, error};

mod avg;
mod count;
mod last;
mod max;
mod min;
mod sum;
mod mul;
mod closure;

pub use avg::Avg;
pub use count::Count;
pub use last::Last;
pub use max::Max;
pub use min::Min;
pub use sum::Sum;
pub use mul::Mul;
pub use closure::Closure;

/// Aggregates used while reducing must implement this trait.
pub trait Aggregate {
    /// Updates the current value with the next row of data.
    fn update(&mut self, headers: &Headers, row: &Row) -> error::Result<()>;

    /// Gets the current value.
    fn value(&self) -> String;

    /// Gets this aggregate's colname
    fn colname(&self) -> &str;
}
