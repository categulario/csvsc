use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

use crate::{Headers, Row};

/// Types implementing this trait can be used to group rows in a row stream,
/// both for [`group()`](crate::RowStream::group) and
/// [`adjacent_group()`](crate::RowStream::adjacent_group)
pub trait GroupCriteria {
    /// Compute the hash of a given row as u64.
    fn group_for(&self, headers: &Headers, row: &Row) -> u64;
}

impl GroupCriteria for [&str] {
    fn group_for(&self, headers: &Headers, row: &Row) -> u64 {
        let mut hasher = DefaultHasher::new();

        self
            .iter()
            .filter_map(|col| headers.get_field(row, col))
            .for_each(|c| c.hash(&mut hasher));

        hasher.finish()
    }
}

impl<const N: usize> GroupCriteria for [&str; N] {
    fn group_for(&self, headers: &Headers, row: &Row) -> u64 {
        GroupCriteria::group_for(&self[..], headers, row)
    }
}

impl<H, F> GroupCriteria for F
where
    F: Fn(&Headers, &Row) -> H,
    H: Hash,
{
    fn group_for(&self, headers: &Headers, row: &Row) -> u64 {
        let mut hasher = DefaultHasher::new();

        let hashable = (self)(headers, row);

        hashable.hash(&mut hasher);

        hasher.finish()
    }
}

#[cfg(test)]
mod tests {
    use super::GroupCriteria;

    use crate::{Headers, Row};

    #[test]
    fn test_slice_of_columns_hash() {
        let group = &["a"];
        let data = Row::from(vec!["1", "2"]);
        let headers = Headers::from(Row::from(vec!["a", "b"]));

        let hash = group.group_for(&headers, &data);

        let data = Row::from(vec!["1", "3"]);

        assert_eq!(
            group.group_for(&headers, &data),
            hash
        );
    }

    #[test]
    fn test_closure_hash_naked() {
        let group = |headers: &Headers, row: &Row| {
            headers.get_field(row, "a").unwrap().to_string()
        };
        let data = Row::from(vec!["1", "2"]);
        let headers = Headers::from(Row::from(vec!["a", "b"]));

        let hash = group.group_for(&headers, &data);

        let data = Row::from(vec!["1", "3"]);

        assert_eq!(
            group.group_for(&headers, &data),
            hash
        );
    }

    #[test]
    fn test_array_hash() {
        let group = ["a"];
        let data = Row::from(vec!["1", "2"]);
        let headers = Headers::from(Row::from(vec!["a", "b"]));

        let hash = group.group_for(&headers, &data);

        let data = Row::from(vec!["1", "3"]);

        assert_eq!(
            group.group_for(&headers, &data),
            hash
        );
    }
}
